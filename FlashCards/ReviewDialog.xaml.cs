﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;

namespace FlashCards
{
    /// <summary>
    /// Interaction logic for ReviewDialog.xaml
    /// </summary>
    public partial class ReviewDialog : Window
    {
        Deck CurrDeck;
        List<Progress> ProgressesList = new List<Progress>();
        List<Progress> ProgressesToLearnList = new List<Progress>();
        Card CurrCard;
        User CurrUser;
        LearningSession CurrLearningSession;
        Random Rnd = new Random();
        bool IsDeckFinished = false;
        bool NeedMoreCars = false;
        int IndexAnswerOne;
        int IndexAnswerTwo;
        int IndexAnswerThree;
        int CurrentIndex = 0;
        int LevelOne = -1;
        int LevelTwo;
        int LevelThree;
        int LevelFour;
        int LevelFive = -2;
        Storyboard ProgressBar;
        String Level;
        public ReviewDialog(Deck deck, User user)
        {
            InitializeComponent();

            ProgressBar = (Storyboard)(this.FindName("progressBar"));

            CurrDeck = deck;
            CurrUser = user;
            tbDeckName.Text = CurrDeck.DeckName;
            CurrLearningSession = CurrDeck.LearningSessions.Where(l => l.UserId == CurrUser.Id).FirstOrDefault();
            CurrLearningSession.Session++;
            if (CurrLearningSession.Session > 9) CurrLearningSession.Session = 0;
            GetCardsToLearn();

            if (NeedMoreCars) { return; }
            if (IsDeckFinished)
            {
                if (!Utils.ShowMessge("Congratulations! You have learned all the cards in this deck. Do you want to learn again?", "Information", Globals.Mw))
                { return; }
                try
                {
                    CurrLearningSession.Session = -1;
                    CurrLearningSession.LastReview = DateTime.Now;
                    ProgressesList.ForEach(p => { p.Level = -1; });
                    Globals.ctx.SaveChanges();
                    return;
                }
                catch (Exception ex) when (ex is SystemException)
                {
                    Utils.ShowMessge(ex.Message, "Error", Globals.Mw);
                }
            }
            while (ProgressesToLearnList.Count() == 0)
            {
                CurrLearningSession.Session++;
                if (CurrLearningSession.Session > 9)
                {
                    CurrLearningSession.Session = 0;
                }
                GetCardsToLearn();
            }
            LoadCard(CurrentIndex);
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        public void SetLevel(String level)
        {
            if (level == "easy")
            {
                ProgressBar.Stop(this);
                pbInterval.IsEnabled = false;
                pbInterval.Visibility = Visibility.Hidden;
            }
            if (level == "hard")
            {
                ProgressBar.SpeedRatio = 2;
            }
        }

        private void TimeIsUp()
        {

            btnStart.IsEnabled = true;
            tbTimeIsUp.Visibility = Visibility.Visible;
            IncorrectAnswer();
            rbAnswer1.Foreground = (rbAnswer1.Content + "" == ProgressesToLearnList[IndexAnswerOne].Card.BackText)
                ? (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF7CDB8D"))
                : rbAnswer1.Foreground;
            rbAnswer2.Foreground = (rbAnswer2.Content + "" == ProgressesToLearnList[IndexAnswerOne].Card.BackText)
                ? (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF7CDB8D"))
                : rbAnswer2.Foreground;
            rbAnswer3.Foreground = (rbAnswer3.Content + "" == ProgressesToLearnList[IndexAnswerOne].Card.BackText)
                ? (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF7CDB8D"))
                : rbAnswer3.Foreground;
            rbAnswer1.IsEnabled = false;
            rbAnswer2.IsEnabled = false;
            rbAnswer3.IsEnabled = false;
            pbInterval.Value = 0;
            if (ProgressesToLearnList.Count() == 1)
            {
                btnStart.Content = "Finish";
            }
            else
            {
                btnStart.Content = "Next";
            }
        }

        private void LoadCard(int index)
        {
            try
            {
                CurrCard = ProgressesToLearnList[index].Card;
                frontCardImageText.Text = CurrCard.FrontText;
                BitmapImage bitmapFrontImage = Utils.GetBitmapImage(CurrCard.FrontImage);
                frontCardImage.Source = bitmapFrontImage;
                GetRandomAnswers();
            }
            catch (Exception ex) when (ex is SystemException)
            {
                Utils.ShowMessge(ex.Message, "Error", this);
            }
        }

        private void GetCardsToLearn()
        {

            ProgressesList = CurrLearningSession.Progresses.ToList();
            if (ProgressesList.Count < 4)
            {
                Utils.ShowMessge("Add more cards in the deck (at least 4 cards)", "Information", Globals.Mw);
                NeedMoreCars = true;
                return;
            }
            LevelTwo = CurrLearningSession.Session + 8;
            if (LevelTwo > 9) LevelTwo -= 10;
            LevelThree = CurrLearningSession.Session + 5;
            if (LevelThree > 9) LevelThree -= 10;
            LevelFour = CurrLearningSession.Session + 1;
            if (LevelFour > 9) LevelFour -= 10;
            ProgressesList.ForEach(p =>
            {
                if (p.Level == LevelOne || p.Level == LevelTwo || p.Level == LevelThree || p.Level == LevelFour)
                {
                    ProgressesToLearnList.Add(p);
                }
            });
            if (ProgressesList.Where(p => p.Level == -2).ToList().Count == ProgressesList.Count)
            {
                IsDeckFinished = true;
            }
            tbCardsInDeck.Text = ProgressesToLearnList.Count() + "";
            CurrentIndex = Rnd.Next(0, ProgressesToLearnList.Count());

        }

        public void GetRandomAnswers()
        {
            IndexAnswerOne = CurrentIndex;

            if (ProgressesToLearnList.Count() < 3)
            {
                do
                {
                    IndexAnswerTwo = Rnd.Next(0, ProgressesList.Count());
                }
                while (ProgressesList[IndexAnswerTwo].Card.BackText == ProgressesToLearnList[IndexAnswerOne].Card.BackText);
                do
                {
                    IndexAnswerThree = Rnd.Next(0, ProgressesList.Count());
                }
                while (ProgressesList[IndexAnswerThree].Card.BackText == ProgressesToLearnList[CurrentIndex].Card.BackText ||
                ProgressesList[IndexAnswerThree].Card.BackText == ProgressesList[IndexAnswerTwo].Card.BackText);
                List<string> answersAltList = new List<string>();
                List<int> randomAltList = new List<int>();
                while (randomAltList.Count < 3)
                {
                    int randomNumber = Rnd.Next(3);
                    if (!randomAltList.Contains(randomNumber))
                        randomAltList.Add(randomNumber);
                }

                answersAltList.Add(ProgressesToLearnList[IndexAnswerOne].Card.BackText);
                answersAltList.Add(ProgressesList[IndexAnswerTwo].Card.BackText);
                answersAltList.Add(ProgressesList[IndexAnswerThree].Card.BackText);

                rbAnswer1.Content = answersAltList[randomAltList[0]];
                rbAnswer2.Content = answersAltList[randomAltList[1]];
                rbAnswer3.Content = answersAltList[randomAltList[2]];
            }
            else
            {
                do
                {
                    IndexAnswerTwo = Rnd.Next(0, ProgressesToLearnList.Count());
                }
                while (IndexAnswerTwo == IndexAnswerOne);
                do
                {
                    IndexAnswerThree = Rnd.Next(0, ProgressesToLearnList.Count());
                }
                while (IndexAnswerThree == CurrentIndex || IndexAnswerThree == IndexAnswerTwo);

                List<string> answersList = new List<string>();
                List<int> randomList = new List<int>();
                while (randomList.Count < 3)
                {
                    int randomNumber = Rnd.Next(3);
                    if (!randomList.Contains(randomNumber))
                        randomList.Add(randomNumber);
                }

                answersList.Add(ProgressesToLearnList[IndexAnswerOne].Card.BackText);
                answersList.Add(ProgressesToLearnList[IndexAnswerTwo].Card.BackText);
                answersList.Add(ProgressesToLearnList[IndexAnswerThree].Card.BackText);

                rbAnswer1.Content = answersList[randomList[0]];
                rbAnswer2.Content = answersList[randomList[1]];
                rbAnswer3.Content = answersList[randomList[2]];
            }
        }

        public void NextCard()
        {
            if (ProgressesToLearnList.Count() == 0) { return; }
            ProgressesToLearnList.Remove(ProgressesToLearnList[CurrentIndex]);
            tbCardsInDeck.Text = ProgressesToLearnList.Count() + "";
            CurrentIndex = Rnd.Next(0, ProgressesToLearnList.Count());
            LoadCard(CurrentIndex);
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            if (btnStart.Content.Equals("Finish"))
            {
                try
                {
                    CurrLearningSession.LastReview = DateTime.Now;
                    if (CurrLearningSession.Session > 9)
                    {
                        CurrLearningSession.Session = 0;
                    }
                    Globals.ctx.SaveChanges();
                    DialogResult = true;
                }
                catch (SystemException ex)
                {
                    Utils.ShowMessge(ex.Message, "Database operation failed", this);
                }
            }
            else if (btnStart.Content.Equals("Stop"))
            {
                ProgressBar.Pause(this);
                btnStart.Content = "Continue";
            }
            else if (btnStart.Content.Equals("Continue"))
            {
                ProgressBar.Resume(this);
                btnStart.Content = "Stop";
            }
            else if (btnStart.Content.Equals("Next"))
            {
                if (ProgressesToLearnList.Count() == 1)
                {
                    btnStart.Content = "Finish";
                    return;
                }
                ProgressBar.Begin(this, true);
                pbInterval.Value = 0;
                tbCorrect.Visibility = Visibility.Hidden;
                tbIncorrect.Visibility = Visibility.Hidden;
                tbTimeIsUp.Visibility = Visibility.Hidden;
                NextCard();
                rbAnswer1.IsChecked = false;
                rbAnswer2.IsChecked = false;
                rbAnswer3.IsChecked = false;
                rbAnswer1.IsEnabled = true;
                rbAnswer2.IsEnabled = true;
                rbAnswer3.IsEnabled = true;
                rbAnswer1.Foreground = Brushes.White;
                rbAnswer2.Foreground = Brushes.White;
                rbAnswer3.Foreground = Brushes.White;
                if (Level != "easy")
                {
                    btnStart.Content = "Next";
                    btnStart.IsEnabled = false;
                }
                else
                {
                    btnStart.Content = "Next";
                }
            }
            else if (btnStart.Content.Equals("Start"))
            {
                RadioGroupLevels.Visibility = Visibility.Hidden;
                SetLevel(Level);
                if (Level != "easy")
                {
                    ProgressBar.Completed += (s, eArgs) => TimeIsUp();
                    pbInterval.Visibility = Visibility.Visible;
                    ProgressBar.Begin(this, true);
                }
                btnStart.Content = "Next";
                btnStart.IsEnabled = false;
                rbAnswer1.Visibility = Visibility.Visible;
                rbAnswer2.Visibility = Visibility.Visible;
                rbAnswer3.Visibility = Visibility.Visible;
            }
        }

        private void CorrectAnswer()
        {
            tbCorrect.Visibility = Visibility.Visible;
            if (ProgressesToLearnList[CurrentIndex].Level == LevelFour)
            {
                ProgressesToLearnList[CurrentIndex].Level = LevelFive;
            }
            if (ProgressesToLearnList[CurrentIndex].Level == LevelOne)
            {
                ProgressesToLearnList[CurrentIndex].Level = CurrLearningSession.Session;
            }
        }

        private void IncorrectAnswer()
        {
            if (tbTimeIsUp.Visibility != Visibility.Visible)
            {
                tbIncorrect.Visibility = Visibility.Visible;
            }
            rbAnswer1.Foreground = rbAnswer1.IsChecked == true
                ? (SolidColorBrush)(new BrushConverter().ConvertFrom("#ff8080"))
                : rbAnswer1.Foreground;
            rbAnswer2.Foreground = rbAnswer2.IsChecked == true
                ? (SolidColorBrush)(new BrushConverter().ConvertFrom("#ff8080"))
                : rbAnswer2.Foreground;
            rbAnswer3.Foreground = rbAnswer3.IsChecked == true
                ? (SolidColorBrush)(new BrushConverter().ConvertFrom("#ff8080"))
                : rbAnswer3.Foreground;
            ProgressesToLearnList[CurrentIndex].Level = LevelOne;

        }


        private void rbAnswer_Checked(object sender, RoutedEventArgs e)
        {
            ProgressBar.Stop(this);
            pbInterval.Value = 0;
            btnStart.IsEnabled = true;
            rbAnswer1.IsEnabled = false;
            rbAnswer2.IsEnabled = false;
            rbAnswer3.IsEnabled = false;
            if (ProgressesToLearnList.Count() == 1)
            {
                btnStart.Content = "Finish";
            }
            else
            {
                btnStart.Content = "Next";
            }
            if (rbAnswer1.Content + "" == ProgressesToLearnList[IndexAnswerOne].Card.BackText)
            {
                rbAnswer1.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF7CDB8D"));
                if (rbAnswer1.IsChecked == true)
                {
                    CorrectAnswer();
                }
            }
            else if (rbAnswer2.Content + "" == ProgressesToLearnList[IndexAnswerOne].Card.BackText)
            {
                rbAnswer2.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF7CDB8D"));
                if (rbAnswer2.IsChecked == true)
                {
                    CorrectAnswer();
                }
            }
            else if (rbAnswer3.Content + "" == ProgressesToLearnList[IndexAnswerOne].Card.BackText)
            {
                rbAnswer3.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF7CDB8D"));
                if (rbAnswer3.IsChecked == true)
                {
                    CorrectAnswer();
                }
            }
            if (tbCorrect.Visibility != Visibility.Visible)
            {
                IncorrectAnswer();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (IsDeckFinished || NeedMoreCars)
            {
                DialogResult = true;
            }
        }

        private void radio_Checked(object sender, RoutedEventArgs e)
        {
            if (radioHard == null) { return; }
            Level = radioEasy.IsChecked == true ? "easy" : Level;
            Level = radioNormal.IsChecked == true ? "normal" : Level;
            Level = radioHard.IsChecked == true ? "hard" : Level;
        }
    }
}
