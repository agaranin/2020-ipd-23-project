﻿using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace FlashCards
{
    /// <summary>
    /// Interaction logic for ViewDeckDialog.xaml
    /// </summary>
    public partial class ViewDeckDialog : Window
    {
        bool CardIsFront = true;
        Deck CurrDeck;
        List<Card> CardsList = new List<Card>();
        Card CurrCard;
        int CurrentIndex = 0;
        public ViewDeckDialog(Deck deck)
        {
            InitializeComponent();
            CurrDeck = deck;
            GetCurrentDeckCards();

            tbDeckName.Text = CurrDeck.DeckName;
            cbPublic.IsChecked = CurrDeck.IsPublic;
            if (CurrDeck.CardsInDeck == 0)
            {
                tbFrontNoCards.Visibility = Visibility.Visible;
                tbBackNoCards.Visibility = Visibility.Visible;
            }
            else
            {
                LoadCard(CurrentIndex);
            }

        }
        private void GetCurrentDeckCards()
        {
            CardsList = CurrDeck.Cards.ToList<Card>();
            Binding bindingCardsInDeck = new Binding("CardsInDeck");
            bindingCardsInDeck.Source = CurrDeck;
            tbCardsInDeck.SetBinding(TextBlock.TextProperty, bindingCardsInDeck);
        }

        private void BindCurrentCardIndex(int index)
        {
            Binding bindingCurrentCard = new Binding();
            index++;
            bindingCurrentCard.Source = index;
            tbCurrentCard.SetBinding(TextBlock.TextProperty, bindingCurrentCard);
        }

        private void LoadCard(int index)
        {
            try
            {
                CurrCard = CardsList[index];
                frontCardImageText.Text = CurrCard.FrontText;
                backCardImageText.Text = CurrCard.BackText;
                frontCardImageText.VerticalAlignment = CurrCard.FrontImage == null ? VerticalAlignment.Center : VerticalAlignment.Top;
                backCardImageText.VerticalAlignment = CurrCard.BackImage == null ? VerticalAlignment.Center : VerticalAlignment.Top;
                BitmapImage bitmapFrontImage = Utils.GetBitmapImage(CurrCard.FrontImage);
                frontCardImage.Source = bitmapFrontImage;
                BitmapImage bitmapBackImage = Utils.GetBitmapImage(CurrCard.BackImage);
                backCardImage.Source = bitmapBackImage;
                BindCurrentCardIndex(index);
            }
            catch (Exception ex) when (ex is SystemException)
            {
                Utils.ShowMessge(ex.Message, "Error", this);
            }
        }

        private void ButtonFlipCard_Click(object sender, RoutedEventArgs e)
        {
            if (CardIsFront)
            {
                FrontCard.Visibility = Visibility.Hidden;
                BackCard.Visibility = Visibility.Visible;
                CardIsFront = false;
            }
            else
            {
                FrontCard.Visibility = Visibility.Visible;
                BackCard.Visibility = Visibility.Hidden;
                CardIsFront = true;
            }
        }

        private void ButtonAddCard_Click(object sender, RoutedEventArgs e)
        {
            AddCardDialog addCardDialog = new AddCardDialog(CurrDeck, null) { Owner = this };
            if (addCardDialog.ShowDialog() == true)
            {
                tbFrontNoCards.Visibility = Visibility.Hidden;
                tbBackNoCards.Visibility = Visibility.Hidden;
                GetCurrentDeckCards();
                CurrentIndex = CurrDeck.CardsInDeck - 1;
                LoadCard(CurrentIndex);
            }
        }

        private void ButtonNext_Click(object sender, RoutedEventArgs e)
        {
            if (CurrDeck.CardsInDeck == 0) { return; }
            if (CurrentIndex < CurrDeck.CardsInDeck - 1)
            {
                CurrentIndex++;
                LoadCard(CurrentIndex);
            }
            else
            {
                CurrentIndex = 0;
                LoadCard(CurrentIndex);
            }
        }

        private void ButtonBack_Click(object sender, RoutedEventArgs e)
        {
            if (CurrDeck.CardsInDeck == 0) { return; }
            if (CurrentIndex > 0)
            {
                CurrentIndex--;
                LoadCard(CurrentIndex);
            }
            else
            {
                CurrentIndex = CurrDeck.CardsInDeck - 1;
                LoadCard(CurrentIndex);
            }
        }

        private void ButtonEditCard_Click(object sender, RoutedEventArgs e)
        {
            AddCardDialog addCardDialog = new AddCardDialog(CurrDeck, CurrCard) { Owner = this };
            addCardDialog.ShowDialog();
            GetCurrentDeckCards();
            LoadCard(CurrentIndex);
        }

        private void ButtonDeleteCard_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!Utils.ShowMessge("Delete the card?", "Warning", this))
                { return; }
                CurrCard.Progresses.Clear();
                CurrDeck.Cards.Remove(CurrCard);
                Globals.ctx.SaveChanges();
                GetCurrentDeckCards();
                if (CurrentIndex != 0) { CurrentIndex--; }
                else
                {
                    frontCardImageText.Text = "";
                    backCardImageText.Text = "";
                    frontCardImage.Source = null;
                    backCardImage.Source = null;
                    tbFrontNoCards.Visibility = Visibility.Visible;
                    tbBackNoCards.Visibility = Visibility.Visible;
                    BindCurrentCardIndex(-1);
                    return;
                }
                LoadCard(CurrentIndex);
            }
            catch (SystemException ex)
            {
                Utils.ShowMessge(ex.Message, "Database operation failed", this);
            }
        }

        private void ButtonDone_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CurrDeck.DeckName = tbDeckName.Text;
                CurrDeck.IsPublic = cbPublic.IsChecked == true ? true : false;
                DialogResult = true;
                Globals.ctx.SaveChanges();
            }
            catch (SystemException ex)
            {
                Utils.ShowMessge(ex.Message, "Database operation failed", this);
            }
        }

        private void ButtonExport_Click(object sender, RoutedEventArgs e)
        {
            // using Json.NET framework  (https://www.newtonsoft.com/json/help/html/Introduction.htm)
            try
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Json file (*.json)|*.json";
                saveFileDialog.Title = "Export to file";
                if (saveFileDialog.ShowDialog() == false) { return; }

                // Create a new custom Contract Resolver
                var customContractResolver = new IgnorableSerializerContractResolver();

                //Specify classes and properties to mark them with the [JsonIgnore] attribute
                customContractResolver.Ignore(typeof(Deck), "Id");
                customContractResolver.Ignore(typeof(Deck), "OwnerId");
                customContractResolver.Ignore(typeof(Deck), "IsPublic");
                customContractResolver.Ignore(typeof(Deck), "User");
                customContractResolver.Ignore(typeof(Deck), "LearningSessions");
                customContractResolver.Ignore(typeof(Deck), "CardsInDeck");
                customContractResolver.Ignore(typeof(Deck), "LastReviewDate");
                customContractResolver.Ignore(typeof(Deck), "ProgressBar");
                customContractResolver.Ignore(typeof(Deck), "OwnerName");
                customContractResolver.Ignore(typeof(Deck), "LevelsList");
                customContractResolver.Ignore(typeof(Card), "Id");
                customContractResolver.Ignore(typeof(Card), "DeckId");
                customContractResolver.Ignore(typeof(Card), "OwnerId");
                customContractResolver.Ignore(typeof(Card), "Deck");
                customContractResolver.Ignore(typeof(Card), "Progresses");

                // Create new JsonSerializerSettings object to specify our custom ContractResolver
                var jsonSettings = new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore, ContractResolver = customContractResolver };

                // Serialize Deck object to the JSON string
                var json = JsonConvert.SerializeObject(CurrDeck, jsonSettings);

                // Write json string to the file
                string fileName = saveFileDialog.FileName;
                File.WriteAllText(fileName, json);
            }
            catch (SystemException ex)
            {
                Utils.ShowMessge(ex.Message, "Database operation failed", this);
            }
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }
    }
}
