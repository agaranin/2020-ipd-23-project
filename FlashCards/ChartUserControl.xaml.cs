﻿using LiveCharts;
using LiveCharts.Wpf;
using System;
using System.Windows;
using System.Windows.Controls;

namespace FlashCards
{
    /// <summary>
    /// Interaction logic for ChartUserControl.xaml
    /// </summary>
    public partial class ChartUserControl : UserControl
    {
        Deck CurrDeck;
        public ChartUserControl(Deck selectedDeck)
        {
            InitializeComponent();
            CurrDeck = selectedDeck;

            PointLabel = chartPoint =>
                string.Format("{0} ({1:P})", chartPoint.Y, chartPoint.Participation);
            PointLabelNull = chartPoint =>
                string.Format("", chartPoint.Y, chartPoint.Participation);
            DataContext = this;
            var point = CurrDeck.LevelsList[1] == 0 ? PointLabelNull : PointLabel;
            var point2 = CurrDeck.LevelsList[2] == 0 ? PointLabelNull : PointLabel;
            var point3 = CurrDeck.LevelsList[3] == 0 ? PointLabelNull : PointLabel;
            var point4 = CurrDeck.LevelsList[4] == 0 ? PointLabelNull : PointLabel;
            var point5 = CurrDeck.LevelsList[5] == 0 ? PointLabelNull : PointLabel;
            SeriesCollection = new SeriesCollection
            {
                new PieSeries
                {
                    Title = "New or Wrong answers",
                    Values = new ChartValues<double> { CurrDeck.LevelsList[1] },
                    DataLabels = true,
                    LabelPoint= point,

                },
                new PieSeries
                {
                    Title = "Reviewed once",
                    Values = new ChartValues<double> { CurrDeck.LevelsList[2] },
                    DataLabels = true,
                    LabelPoint= point2
                },
                new PieSeries
                {
                    Title = "Reviewed twice",
                    Values = new ChartValues<double> { CurrDeck.LevelsList[3] },
                    DataLabels = true,
                    LabelPoint= point3
                },
                new PieSeries
                {
                    Title = "Reviewed thrice",
                    Values = new ChartValues<double> { CurrDeck.LevelsList[4] },
                    DataLabels = true,
                    LabelPoint= point4
                },
                new PieSeries
                {
                    Title = "Finished ",
                    Values = new ChartValues<double> { CurrDeck.LevelsList[5] },
                    DataLabels = true,
                    LabelPoint= point5
                }
            };
        }
        public SeriesCollection SeriesCollection { get; set; }
        public Func<ChartPoint, string> PointLabel { get; set; }
        public Func<ChartPoint, string> PointLabelNull { get; set; }

        private void Chart_OnDataClick(object sender, ChartPoint chartpoint)
        {
            var chart = (LiveCharts.Wpf.PieChart)chartpoint.ChartView;

            //clear selected slice.
            foreach (PieSeries series in chart.Series)
                series.PushOut = 0;

            var selectedSeries = (PieSeries)chartpoint.SeriesView;
            selectedSeries.PushOut = 8;
        }

        private void ButtonClose_Click(object sender, RoutedEventArgs e)
        {
            MainUserControl mainUserControl = new MainUserControl();
            Globals.Mw.GridCommonSpace.Children.Clear();
            Globals.Mw.GridCommonSpace.Children.Add(mainUserControl);

        }
    }
}
