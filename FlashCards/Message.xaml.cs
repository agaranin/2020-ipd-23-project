﻿using System.Windows;
using System.Windows.Input;

namespace FlashCards
{
    /// <summary>
    /// Interaction logic for Message.xaml
    /// </summary>
    public partial class Message : Window
    {
        public string Param { get; private set; }

        public Message()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        public void ShowMessage(string message, string title)
        {
            tbMessageBox.Text = message;
            this.Title = title;
        }

        public void ShowMessage(string message, string title, string param)
        {
            tbMessageBox.Text = message;
            this.Title = title;
            this.Param = param;
        }

        public void ShowMessage(string text)
        {
            tbMessageBox.Text = text;
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }
    }
}
