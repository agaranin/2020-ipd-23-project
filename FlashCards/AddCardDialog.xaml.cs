﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace FlashCards
{
    /// <summary>
    /// Interaction logic for AddCardDialog.xaml
    /// </summary>
    public partial class AddCardDialog : Window
    {
        byte[] FrontSideImage;
        byte[] BackSideImage;
        Deck CurrDeck;
        Card CurrCard;
        public AddCardDialog(Deck deck, Card card)
        {
            InitializeComponent();
            CurrDeck = deck;
            CurrCard = card;
            if (CurrCard != null)
            {
                tbFront.Text = CurrCard.FrontText;
                FrontSideImage = CurrCard.FrontImage;
                BitmapImage bitmapFrontImage = Utils.GetBitmapImage(FrontSideImage);
                frontImageViewer.Source = bitmapFrontImage;
                tbBack.Text = CurrCard.BackText;
                BackSideImage = CurrCard.BackImage;
                BitmapImage bitmapBackImage = Utils.GetBitmapImage(BackSideImage);
                backImageViewer.Source = bitmapBackImage;
                BtnAddEdit.Content = "Update";
                Title = "Edit card";
            }
        }

        private void ButtonNext_Click(object sender, RoutedEventArgs e)
        {
            FrontCard.Visibility = System.Windows.Visibility.Hidden;
            BackCard.Visibility = System.Windows.Visibility.Visible;
        }

        private void ButtonBack_Click(object sender, RoutedEventArgs e)
        {
            FrontCard.Visibility = System.Windows.Visibility.Visible;
            BackCard.Visibility = System.Windows.Visibility.Hidden;
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CurrCard != null)
                {

                    CurrCard.FrontText = tbFront.Text;
                    CurrCard.FrontImage = FrontSideImage;
                    CurrCard.BackText = tbBack.Text;
                    CurrCard.BackImage = BackSideImage;
                }
                else
                {
                    Card card = new Card
                    {
                        FrontText = tbFront.Text,
                        FrontImage = FrontSideImage,
                        BackText = tbBack.Text,
                        BackImage = BackSideImage,
                    };
                    CurrDeck.Cards.Add(card);

                    Progress progress = new Progress
                    {
                        CardId = card.Id,
                        Level = -1,
                    };
                    CurrDeck.LearningSessions.ToList().ForEach(l => l.Progresses.Add(progress));
                }
                Globals.ctx.SaveChanges();
                DialogResult = true;
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ButtonFrontImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Image files (*.jpg;*.jpeg;*.gif;*.png)|*.jpg;*.jpeg;*.gif;*.png|All Files (*.*)|*.*";
            dlg.RestoreDirectory = true;

            if (dlg.ShowDialog() == true)
            {
                try
                {
                    FrontSideImage = File.ReadAllBytes(dlg.FileName);
                    tbFrontImage.Visibility = Visibility.Hidden;
                    BitmapImage bitmap = Utils.GetBitmapImage(FrontSideImage); // ex: SystemException
                    frontImageViewer.Source = bitmap;
                }
                catch (Exception ex) when (ex is SystemException || ex is IOException)
                {
                    MessageBox.Show(ex.Message, "File reading failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        private void ButtonBackImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Image files (*.jpg;*.jpeg;*.gif;*.png)|*.jpg;*.jpeg;*.gif;*.png|All Files (*.*)|*.*";
            dlg.RestoreDirectory = true;

            if (dlg.ShowDialog() == true)
            {
                try
                {
                    BackSideImage = File.ReadAllBytes(dlg.FileName);
                    tbBackImage.Visibility = Visibility.Hidden;
                    BitmapImage bitmap = Utils.GetBitmapImage(BackSideImage); // ex: SystemException
                    backImageViewer.Source = bitmap;
                }
                catch (Exception ex) when (ex is SystemException || ex is IOException)
                {
                    MessageBox.Show(ex.Message, "File reading failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }
    }
}
