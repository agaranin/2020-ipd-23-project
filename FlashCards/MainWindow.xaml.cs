﻿using Microsoft.Win32;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace FlashCards
{

    public partial class MainWindow : Window
    {
        MainUserControl mainUserControl;
        DispatcherTimer Timer;
        public MainWindow()
        {
            InitializeComponent();
            Globals.Mw = this;
            mainUserControl = new MainUserControl();
            GridCommonSpace.Children.Add(mainUserControl);
            miSignIn.Visibility = Visibility.Collapsed;
            miLogOut.Visibility = Visibility.Visible;
            ReturnUserName();
            Timer = new DispatcherTimer();
            Clock();
        }
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void Clock()
        {
            Timer.Tick += new EventHandler(Timer_Tick);
            Timer.Interval = new TimeSpan(0, 0, 1);
            Timer.Start();
        }
        private void Timer_Tick(object sender, EventArgs e)
        {
            lblStatus.Content = DateTime.Now.ToString("hh:mm:ss tt");
            CommandManager.InvalidateRequerySuggested();
        }


        private void MenuItem_Share(object sender, RoutedEventArgs e)
        {
            ShareDialog shareDialog = new ShareDialog() { Owner = this };
            shareDialog.Show();
        }

        private void MenuItem_Click_SignUp(object sender, RoutedEventArgs e)
        {
            LogIn();
        }

        private void LogIn()
        {
            Hide();
            LogInDialog logInDialog = new LogInDialog();
            logInDialog.UserFound += (user) => { Globals.CurrUser = user; };
            if (logInDialog.ShowDialog() == false) { Close(); return; }
            Show();
            miSignIn.Visibility = Visibility.Collapsed;
            miLogOut.Visibility = Visibility.Visible;
            ReturnUserName();
        }

        private void MenuItem_Click_About(object sender, RoutedEventArgs e)
        {
            AboutDialog aboutDialog = new AboutDialog() { Owner = this };
            aboutDialog.ShowDialog();
        }

        private void miLogOut_Click(object sender, RoutedEventArgs e)
        {
            LogIn();
        }
        private void ReturnUserName()
        {
            tbUserName.Text = "Howdy, " + Globals.CurrUser.Name;
        }

        private void MenuItemNewDeck_Click(object sender, RoutedEventArgs e)
        {
            AddDeckDialog addDeckDialog = new AddDeckDialog(Globals.CurrUser) { Owner = this };
            addDeckDialog.ShowDialog();
            mainUserControl.FetchUserDecks(Globals.CurrUser);
        }

        private void MenuItem_Click_Import(object sender, RoutedEventArgs e)
        {
            // using Json.NET framework  (https://www.newtonsoft.com/json/help/html/Introduction.htm)
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.CheckFileExists = true;
            if (openFileDialog.ShowDialog() == true)
            {
                if (openFileDialog.FileName.Trim() != string.Empty)
                {
                    using (StreamReader sr = new StreamReader(openFileDialog.FileName))
                    {
                        try
                        {
                            // Get JSON string from the file
                            string jsonStr = sr.ReadToEnd();
                            // Convert JSON string to dynamic string 
                            var dynamicJsonString = JsonConvert.DeserializeObject<dynamic>(jsonStr, new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore
                            });
                            // Get JArray of Cards from dynamic string
                            JArray cardsJArray = dynamicJsonString["Cards"];
                            // Get DeckName from JObject (cast JToken with the specified key to string)
                            string deckName = (string)dynamicJsonString["DeckName"];

                            //Create and add a new deck

                            Deck newDeck = new Deck
                            {
                                DeckName = deckName,
                                IsPublic = false
                            };
                            Globals.CurrUser.Decks.Add(newDeck);
                            Globals.ctx.SaveChanges();

                            // Add a new learnigSession to the deck
                            LearningSession learningSession = new LearningSession
                            {
                                UserId = Globals.CurrUser.Id,
                                Session = -1,
                                LastReview = DateTime.Now
                            };
                            newDeck.LearningSessions.Add(learningSession);
                            Globals.ctx.SaveChanges();

                            // Create new cards and add them to the deck
                            List<Card> cardsList = cardsJArray.Select(c => new Card
                            {
                                FrontText = c["FrontText"]?.ToObject<string>(),
                                BackText = c["BackText"]?.ToObject<string>(),
                                FrontAudio = c["FrontAudio"]?.ToObject<byte[]>(),
                                BackAudio = c["BackAudio"]?.ToObject<byte[]>(),
                                FrontImage = c["FrontImage"]?.ToObject<byte[]>(), // null check and convert JToken to byte[]
                                BackImage = c["BackImage"]?.ToObject<byte[]>(),
                            }).ToList();
                            cardsList.ForEach(c => newDeck.Cards.Add(c));
                            Globals.ctx.SaveChanges();

                            // Create new progress for each card in the deck 
                            newDeck.Cards.ToList().ForEach(c =>
                            {
                                Progress progress = new Progress
                                {
                                    CardId = c.Id,
                                    Level = -1,
                                };
                                newDeck.LearningSessions.ToList().ForEach(l => l.Progresses.Add(progress));
                            });
                            Globals.ctx.SaveChanges();

                            // Reload collection of user's decks
                            mainUserControl.FetchUserDecks(Globals.CurrUser);
                            Globals.Mw.GridCommonSpace.Children.Clear();
                            Globals.Mw.GridCommonSpace.Children.Add(mainUserControl);
                        }
                        catch (SystemException ex)
                        {
                            Utils.ShowMessge(ex.Message, "Database operation failed", this);
                        }
                    }
                }
            }
        }

        private void MenuItemBrowseDecks_Click(object sender, RoutedEventArgs e)
        {
            PublicDecksUserControl publicDecksUserControl = new PublicDecksUserControl();
            GridCommonSpace.Children.Add(publicDecksUserControl);
        }

        private void MenuItem_Click_Exit(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void MenuItem_Click_Share(object sender, RoutedEventArgs e)
        {
            ShareDialog shareDialog = new ShareDialog() { Owner = this };
            shareDialog.ShowDialog();
        }

    }
}

