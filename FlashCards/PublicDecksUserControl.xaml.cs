﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace FlashCards
{
    /// <summary>
    /// Interaction logic for PublicDecksUserControl.xaml
    /// </summary>
    public partial class PublicDecksUserControl : UserControl
    {
        Deck CurrDeck;
        public PublicDecksUserControl()
        {
            InitializeComponent();
            FetchPublicDecks();
            if (Globals.CurrUser.Id == 1)
            { gwcDelete.Width = 60; }
            else
            { gwcDelete.Width = 0; }
        }

        public void FetchPublicDecks()
        {
            try
            {
                lvPublicDecks.Items.Clear();
                var publicDecksList = Globals.ctx.Decks.Where(d => d.IsPublic == true).ToList();
                publicDecksList.ForEach(d =>
                {
                    d.OwnerName = Globals.ctx.Users.Find(d.OwnerId).Name;
                    lvPublicDecks.Items.Add(d);
                });
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ButtonClose_Click(object sender, RoutedEventArgs e)
        {
            SetMainUserControl();
        }

        private void SetMainUserControl()
        {
            MainUserControl mainUserControl = new MainUserControl();
            Globals.Mw.GridCommonSpace.Children.Clear();
            Globals.Mw.GridCommonSpace.Children.Add(mainUserControl);
        }

        private void ButtonView_Click(object sender, RoutedEventArgs e)
        {
            ViewDeckDialog viewDeckDialog = new ViewDeckDialog(CurrDeck) { Owner = Globals.Mw };
            if (Globals.CurrUser.Id != 1)
            {
                viewDeckDialog.ButtonAddCard.Visibility = Visibility.Hidden;
                viewDeckDialog.ButtonEditCard.Visibility = Visibility.Hidden;
                viewDeckDialog.ButtonDeleteCard.Visibility = Visibility.Hidden;
                viewDeckDialog.tbDeckName.IsEnabled = false;
                viewDeckDialog.cbPublic.IsEnabled = false;
            }
            viewDeckDialog.ShowDialog();
            FetchPublicDecks();
        }

        private void lvPublicDecks_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvPublicDecks.SelectedIndex == -1 || Globals.CurrUser == null)
            {
                return;
            }
            CurrDeck = (Deck)lvPublicDecks.SelectedItem;
        }

        private void ButtonAddToUser_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LearningSession learningSession = new LearningSession
                {
                    UserId = Globals.CurrUser.Id,
                    DeckId = CurrDeck.Id,
                    OwnerId = CurrDeck.OwnerId,
                    Session = -1,
                    LastReview = DateTime.Now
                };
                CurrDeck.LearningSessions.Add(learningSession);
                CurrDeck.Cards.ToList().ForEach(c =>
                {
                    Progress progress = new Progress
                    {
                        DeckId = CurrDeck.Id,
                        OwnerId = CurrDeck.OwnerId,
                        UserId = Globals.CurrUser.Id,
                        CardId = c.Id,
                        LearningSessionId = learningSession.Id,
                        Level = -1,
                    };
                    c.Progresses.Add(progress);
                });
                Globals.ctx.SaveChanges();
                SetMainUserControl();
            }
            catch (SystemException ex)
            {
                Utils.ShowMessge(ex.Message, Globals.Mw);
            }
        }

        private void ButtonDeletePublicDeck_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!Utils.ShowMessge("Delete deck? The deck cannot be restored.", "Warning", Globals.Mw))
                { return; }
                CurrDeck.LearningSessions.Clear();
                CurrDeck.Cards.ToList().ForEach(c => c.Progresses.Clear());
                CurrDeck.Cards.Clear();
                Globals.ctx.Decks.Remove(CurrDeck);
                Globals.ctx.SaveChanges();
                FetchPublicDecks();
            }
            catch (SystemException ex)
            {
                Utils.ShowMessge(ex.Message, "Database operation failed", Globals.Mw);
            }
        }
    }
}
