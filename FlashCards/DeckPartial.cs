﻿using System;

namespace FlashCards
{
    public partial class Deck
    {
        public int CardsInDeck { get => Cards.Count; }
        public DateTime LastReviewDate { get; set; }
        public double ProgressBar { get; set; }
        public string OwnerName { get; set; }
        public double[] LevelsList { get; set; }
    }
}
