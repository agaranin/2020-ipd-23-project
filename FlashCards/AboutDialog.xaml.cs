﻿using System.Windows;
using System.Windows.Input;

namespace FlashCards
{
    /// <summary>
    /// Interaction logic for AboutDialog.xaml
    /// </summary>
    public partial class AboutDialog : Window
    {
        public AboutDialog()
        {
            InitializeComponent();
        }

        private void Label_MouseLeftButtonDownArtemGaranin(object sender, MouseButtonEventArgs e)
        {
            System.Diagnostics.Process.Start("https://ca.linkedin.com/in/artem-garanin");
        }

        private void Label_MouseLeftButtonDownVladimirTocari(object sender, MouseButtonEventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.linkedin.com/in/vladimir-tocari-3863a6b7/");
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }
    }
}
