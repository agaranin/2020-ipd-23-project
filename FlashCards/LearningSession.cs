//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FlashCards
{
    using System;
    using System.Collections.Generic;
    
    public partial class LearningSession
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public LearningSession()
        {
            this.Progresses = new HashSet<Progress>();
        }
    
        public int Id { get; set; }
        public int UserId { get; set; }
        public int DeckId { get; set; }
        public int OwnerId { get; set; }
        public int Session { get; set; }
        public System.DateTime LastReview { get; set; }
    
        public virtual Deck Deck { get; set; }
        public virtual User User { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Progress> Progresses { get; set; }
    }
}
