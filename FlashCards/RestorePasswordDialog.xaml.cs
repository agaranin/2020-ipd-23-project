﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;

namespace FlashCards
{
    /// <summary>
    /// Interaction logic for RestorePasswordDialog.xaml
    /// </summary>
    public partial class RestorePasswordDialog : Window
    {
        public RestorePasswordDialog()
        {
            InitializeComponent();
        }

        private void Button_Click_Send(object sender, RoutedEventArgs e)
        {
            SqlConnection sqlCon = new SqlConnection(@"Data Source=flashcards-server.database.windows.net;Initial Catalog=FlashCards;Persist Security Info=True;User ID=sqladmin;Password=rootRoot1");
            try
            {
                if (sqlCon.State == ConnectionState.Closed)
                {
                    sqlCon.Open();
                }

                string query = "Select Password from Users WHERE Email=@Email";
                SqlCommand sqlCommand = new SqlCommand(query, sqlCon);
                sqlCommand.CommandType = CommandType.Text;
                sqlCommand.Parameters.AddWithValue("@Email", RestorePasswordDialog_tbEmail.Text);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                string pass;
                if (sqlDataReader.Read())
                {
                    pass = sqlDataReader.GetValue(0).ToString();
                    // send email
                    string email = RestorePasswordDialog_tbEmail.Text;
                    Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                    Match match = regex.Match(email);

                    try
                    {
                        if (!match.Success)
                        {
                            Utils.ShowMessge("Wrong email or empty field. Example: lettersNumbers@letters.letters", this);
                            return;
                        }
                        MailMessage msg = new MailMessage("info.flashcards.game@gmail.com",
                                                          RestorePasswordDialog_tbEmail.Text,
                                                          "Flash cards password recovery",
                                                          "Your password: " + pass);
                        msg.IsBodyHtml = false;
                        SmtpClient sc = new SmtpClient("smtp.gmail.com", 587);
                        sc.UseDefaultCredentials = false;
                        NetworkCredential networkCredintials = new NetworkCredential
                                                          ("info.flashcards.game@gmail.com",
                                                             "TeamWork1");

                        sc.Credentials = networkCredintials;
                        sc.EnableSsl = true;

                        sc.Send(msg);
                        Utils.ShowMessge("Password has been sent to your email", this);
                        Close();
                    }

                    catch (SystemException ex)
                    {
                        Utils.ShowMessge(ex.Message, this);
                    }
                    catch (SmtpException ex)
                    {
                        Utils.ShowMessge(ex.Message, this);
                    }
                }
                else
                {
                    Utils.ShowMessge("This email doesn't exist", this);
                }
            }
            catch (SmtpException ex)
            {
                Utils.ShowMessge(ex.Message, this);
            }
            catch (SystemException ex)
            {
                Utils.ShowMessge(ex.Message, this);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void RestorePasswordDialog_tbEmail_GotFocus(object sender, RoutedEventArgs e)
        {
            if (RestorePasswordDialog_tbEmail.Text == "Email address")
                RestorePasswordDialog_tbEmail.Clear();
        }

        private void RestorePasswordDialog_tbEmail_LostFocus(object sender, RoutedEventArgs e)
        {
            if (RestorePasswordDialog_tbEmail.Text == "")
            {
                RestorePasswordDialog_tbEmail.Text = "Email address";
            }
        }
    }
}