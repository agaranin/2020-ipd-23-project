﻿using System;
using System.Windows;
using System.Windows.Input;

namespace FlashCards
{
    /// <summary>
    /// Interaction logic for AddDeckDialog.xaml
    /// </summary>
    public partial class AddDeckDialog : Window
    {
        User CurrUser;
        Deck CurrDeck;
        public AddDeckDialog(User user)
        {
            InitializeComponent();
            CurrUser = user;
        }

        public bool IsFieldsValid()
        {
            if (AddDeckDialog_tbDeckName.Text.Length < 1)
            {
                MessageBox.Show("Enter Deck Name", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            if (!IsFieldsValid()) { return; }
            if (AddDeckDialog_tbDeckName.Text == "Deck Name")
            {
                Utils.ShowMessge("Deck Name can't be name of the Deck", this);
                return;
            }
            try
            {
                CurrDeck = new Deck
                {
                    DeckName = AddDeckDialog_tbDeckName.Text,
                    IsPublic = AddDeckDialog_cbPublic.IsChecked == true ? true : false
                };
                CurrUser.Decks.Add(CurrDeck);
                LearningSession learningSession = new LearningSession
                {
                    UserId = CurrUser.Id,
                    Session = -1,
                    LastReview = DateTime.Now
                };
                CurrDeck.LearningSessions.Add(learningSession);
                Globals.ctx.SaveChanges();
                DialogResult = true;
            }
            catch (SystemException ex)
            {
                Utils.ShowMessge(ex.Message, "Database operation failed", this);
            }

        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void AddDeckDialog_tbDeckName_GotFocus(object sender, RoutedEventArgs e)
        {
            if (AddDeckDialog_tbDeckName.Text == "Deck Name")
                AddDeckDialog_tbDeckName.Clear();
        }

        private void AddDeckDialog_tbDeckName_LostFocus(object sender, RoutedEventArgs e)
        {
            if (AddDeckDialog_tbDeckName.Text == "")
            {
                AddDeckDialog_tbDeckName.Text = "Deck Name";
            }
        }
    }
}
