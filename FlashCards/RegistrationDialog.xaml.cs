﻿using System;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace FlashCards
{
    /// <summary>
    /// Interaction logic for RegistrationDialog.xaml
    /// </summary>
    public partial class RegistrationDialog : Window
    {
        public event Action<User> UserFound;

        public RegistrationDialog()
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterOwner;
        }


        private void Button_Click_Register(object sender, RoutedEventArgs e)
        {
            borderName.Background = Brushes.White;
            borderEmail.Background = Brushes.White;
            borderPass.Background = Brushes.White;
            try
            {
                User user = new User
                {
                    Name = registrationDialog_tbName.Text,
                    Email = registrationDialog_tbEmail.Text,
                    Password = registrationDialog_tbPassword.Text,
                };
                // check if user with entered email exist in database
                var isUserExist = Globals.ctx.Users.Where(u => u.Email == user.Email).ToList();
                if (isUserExist.Count == 1)
                {
                    //      User user = isUserExist[0];
                    user = isUserExist[0];
                    UserFound?.Invoke(user); // if equals 0
                    Utils.ShowMessge("User with " + user.Email + " exist", this);
                    //   MessageBox.Show("User with " + user.Email + " exist");
                    registrationDialog_tbName.Text = ("");
                    registrationDialog_tbEmail.Text = ("");
                    registrationDialog_tbPassword.Text = ("Password");
                    return;

                }
                else
                {
                    Globals.ctx.Users.Add(user);
                    Globals.ctx.SaveChanges();
                    DialogResult = true;
                }
            }
            catch (DbEntityValidationException ex)
            {
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors);
                foreach (DbValidationError m in errorMessages)
                {
                    if (m.PropertyName == "Name")
                    {
                        borderName.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#ff8080"));
                        tbNameErrorTip.Text = m.ErrorMessage;
                    }
                    if (m.PropertyName == "Email")
                    {
                        borderEmail.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#ff8080"));
                        tbEmailErrorTip.Text = m.ErrorMessage;
                    }
                    if (m.PropertyName == "Password")
                    {
                        borderPass.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#ff8080"));
                        tbPassErrorTip.Text = m.ErrorMessage;
                    }
                    Utils.UndoAll(Globals.ctx);
                }
            }
            catch (SystemException ex)
            {
                Utils.ShowMessge(ex.Message, this);
            }
        }

        public bool IsFieldsValid()
        {
            if (registrationDialog_tbName.Text.Length < 2 || registrationDialog_tbName.Text.Length > 25)
            {
                Utils.ShowMessge("Name must be between 2 and 25 characters", "Validation error", this);
                return false;
            }

            Regex regexName = new Regex(@"^[a-zA-Z]+$");
            if (!regexName.IsMatch(registrationDialog_tbName.Text))
            {
                Utils.ShowMessge("Only letters is permitted in name field", this);
                return false;
            }


            string email = registrationDialog_tbEmail.Text;
            Regex regexEmail = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regexEmail.Match(email);

            if (!match.Success)
            {
                Utils.ShowMessge("Wrong email or empty field. Example: lettersNumbers@letters.letters", this);
                return false;
            }


            Regex regex = new Regex("^(?=.+[A-Za-z0-9])(?=.+\\d)(?=.+[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,}$");
            if (!regex.IsMatch(registrationDialog_tbPassword.Text))
            {
                registrationDialog_tbPassword.Clear();
                Utils.ShowMessge("Password shoud have 8 characters. Should contain number/letters and one special character", this);
                return false;
            }
            return true;
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void Button_Click_Cancel(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void registrationDialog_tbName_LostFocus(object sender, RoutedEventArgs e)
        {
            if (registrationDialog_tbName.Text == "")
            {
                registrationDialog_tbName.Text = "Name";
            }
        }

        private void registrationDialog_tbName_GotFocus(object sender, RoutedEventArgs e)
        {
            if (registrationDialog_tbName.Text == "Name")
                registrationDialog_tbName.Clear();
        }

        private void registrationDialog_tbEmail_GotFocus(object sender, RoutedEventArgs e)
        {
            if (registrationDialog_tbEmail.Text == "Email")
                registrationDialog_tbEmail.Clear();
        }

        private void registrationDialog_tbEmail_LostFocus(object sender, RoutedEventArgs e)
        {
            if (registrationDialog_tbEmail.Text == "")
            {
                registrationDialog_tbEmail.Text = "Email";
            }
        }

        private void registrationDialog_tbPassword_LostFocus(object sender, RoutedEventArgs e)
        {
            if (registrationDialog_tbPassword.Text == "")
            {
                registrationDialog_tbPassword.Text = "Password";
            }
        }

        private void registrationDialog_tbPassword_GotFocus(object sender, RoutedEventArgs e)
        {
            if (registrationDialog_tbPassword.Text == "Password")
                registrationDialog_tbPassword.Clear();
        }
    }
}
