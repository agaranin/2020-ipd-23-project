﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace FlashCards
{
    /// <summary>
    /// Interaction logic for LearnDialog.xaml
    /// </summary>
    public partial class LearnDialog : Window
    {
        Deck CurrDeck;
        List<Card> CardsList = new List<Card>();
        Card CurrCard;
        int CurrentIndex = 0;
        DispatcherTimer Timer = new DispatcherTimer();
        public LearnDialog(Deck deck)
        {
            InitializeComponent();
            CurrDeck = deck;
            tbDeckName.Text = CurrDeck.DeckName;
            GetCurrentDeckCards();
            LoadCard(CurrentIndex);
            ProgressTimer();
        }

        private void LoadCard(int index)
        {
            try
            {
                CurrCard = CardsList[index];
                frontCardImageText.Text = CurrCard.FrontText;
                backCardImageText.Text = CurrCard.BackText;
                frontCardImageText.VerticalAlignment = CurrCard.FrontImage == null ? VerticalAlignment.Center : VerticalAlignment.Bottom;
                backCardImageText.VerticalAlignment = CurrCard.BackImage == null ? VerticalAlignment.Center : VerticalAlignment.Bottom;
                BitmapImage bitmapFrontImage = Utils.GetBitmapImage(CurrCard.FrontImage);
                frontCardImage.Source = bitmapFrontImage;
                BitmapImage bitmapBackImage = Utils.GetBitmapImage(CurrCard.BackImage);
                backCardImage.Source = bitmapBackImage;
                BindCurrentCardIndex(index);
            }
            catch (Exception ex) when (ex is SystemException)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void GetCurrentDeckCards()
        {
            CardsList = CurrDeck.Cards.ToList<Card>();
            Binding bindingCardsInDeck = new Binding("CardsInDeck");
            bindingCardsInDeck.Source = CurrDeck;
            tbCardsInDeck.SetBinding(TextBlock.TextProperty, bindingCardsInDeck);
        }

        private void BindCurrentCardIndex(int index)
        {
            Binding bindingCurrentCard = new Binding();
            index++;
            bindingCurrentCard.Source = index;
            tbCurrentCard.SetBinding(TextBlock.TextProperty, bindingCurrentCard);
        }

        private void ButtonNext_Click(object sender, RoutedEventArgs e)
        {
            NextCard();
        }

        private void ButtonBack_Click(object sender, RoutedEventArgs e)
        {
            if (CurrDeck.CardsInDeck == 0) { return; }
            if (CurrentIndex > 0)
            {
                CurrentIndex--;
                LoadCard(CurrentIndex);
            }
            else
            {
                CurrentIndex = CurrDeck.CardsInDeck - 1;
                LoadCard(CurrentIndex);
            }
            if (!btnStart.Content.Equals("Finish"))
            {
                pbInterval.Value = 0;
            }
        }

        public void NextCard()
        {
            if (CurrDeck.CardsInDeck == 0) { return; }
            if (CurrentIndex < CurrDeck.CardsInDeck - 1)
            {
                CurrentIndex++;
                LoadCard(CurrentIndex);
            }
            else
            {
                CurrentIndex = 0;
                LoadCard(CurrentIndex);
            }
            if (!btnStart.Content.Equals("Finish"))
            {
                pbInterval.Value = 0;
            }
        }

        public void ProgressTimer()
        {
            Timer.Interval = TimeSpan.FromMilliseconds(1);
            Timer.Tick += timer_Tick;
        }

        void timer_Tick(object sender, EventArgs e)
        {
            pbInterval.Value += 0.02;
            if (pbInterval.Value == 5)
            {
                if (CurrentIndex == CurrDeck.CardsInDeck - 1)
                {
                    Timer.Stop();
                    btnStart.Content = "Finish";
                    return;
                }
                pbInterval.Value = 0;
                NextCard();
            }
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            if (btnStart.Content.Equals("Finish"))
            {
                try
                {
                    DialogResult = true;
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            else if (btnStart.Content.Equals("Stop"))
            {
                Timer.Stop();
                btnStart.Content = "Continue";
            }
            else
            {
                Timer.Start();
                btnStart.Content = "Stop";
            }
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }
    }
}
