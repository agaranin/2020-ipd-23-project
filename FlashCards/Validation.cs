﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace FlashCards
{
    public partial class User : IValidatableObject
    {

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!Regex.Match(Name, @"^[a-zA-Z0-9]{2,25}$").Success)
            {
                yield return new ValidationResult("Wrong Name format. 2-25 characters.", new[] { nameof(Name) });
            }
            if (!Regex.Match(Email, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Success)
            {
                yield return new ValidationResult("Wrong email format.", new[] { nameof(Email) });
            }
            if (!Regex.Match(Password, "^(?=.+[A-Za-z0-9])(?=.+\\d)(?=.+[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,}$").Success)
            {
                yield return new ValidationResult("Wrong password format. 8 characters, one number and special symbol.", new[] { nameof(Password) });
            }
        }
    }
}

