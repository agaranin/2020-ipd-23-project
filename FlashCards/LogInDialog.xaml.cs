﻿using System;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace FlashCards
{
    /// <summary>
    /// Interaction logic for LogInDialog.xaml
    /// </summary>
    public partial class LogInDialog : Window
    {
        public event Action<User> UserFound;

        public LogInDialog()
        {
            InitializeComponent();
            Globals.ctx = new FlashCardsDbConnection();
        }

        private void Button_Click_Registration(object sender, RoutedEventArgs e)
        {
            this.Hide();
            RegistrationDialog registrationDialog = new RegistrationDialog() { Owner = this };
            registrationDialog.ShowDialog();
            Show();
        }

        private void Button_Click_Submit(object sender, RoutedEventArgs e)
        {

            try
            {
                var currUser = Globals.ctx.Users.Where(u => u.Email == LogInDialog_tbEmail.Text && u.Password == LogInDialog_tbPassword.Password).ToList();
                if (currUser.Count == 1)
                {
                    Globals.CurrUser = currUser[0];
                    UserFound?.Invoke(Globals.CurrUser); // if equals 0
                    Globals.msg = new Message() { Owner = this };
                    MainWindow mainWindow = new MainWindow();
                    mainWindow.Show();
                    this.Close();
                }
                else
                {
                    Utils.ShowMessge("Username or Password in incorrect", this);
                }
            }
            catch (Exception ex)
            {
                Utils.ShowMessge(ex.Message, this);
            }
        }

        private void Button_Click_RestorePassword(object sender, RoutedEventArgs e)
        {
            this.Hide();
            RestorePasswordDialog restorePassword = new RestorePasswordDialog() { Owner = this };
            restorePassword.ShowDialog();
            this.ShowDialog();
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void Button_Click_Exit(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void LogInDialog_tbEmail_GotFocus(object sender, RoutedEventArgs e)
        {
            if (LogInDialog_tbEmail.Text == "Email")
                LogInDialog_tbEmail.Clear();
        }

        private void LogInDialog_tbPassword_GotFocus(object sender, RoutedEventArgs e)
        {
            if (LogInDialog_tbPassword.Password == "00000000")
                LogInDialog_tbPassword.Clear();
        }

        private void LogInDialog_tbEmail_LostFocus(object sender, RoutedEventArgs e)
        {
            if (LogInDialog_tbEmail.Text == "")
            {
                LogInDialog_tbEmail.Text = "Email";
            }
        }

        private void LogInDialog_tbPassword_LostFocus(object sender, RoutedEventArgs e)
        {
            if (LogInDialog_tbPassword.Password == "")
            {
                LogInDialog_tbPassword.Password = "00000000";
            }
        }
    }
}
