﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace FlashCards
{
    public partial class MainUserControl : UserControl
    {
        Deck CurrDeck;
        public MainUserControl()
        {
            InitializeComponent();
            try
            {
                FetchUserDecks(Globals.CurrUser);

            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        public void FetchUserDecks(User user)
        {
            try
            {
                lvDecks.Items.Clear();
                var learningSessionsList = Globals.ctx.LearningSessions.Where(u => u.UserId == Globals.CurrUser.Id).ToList();
                learningSessionsList.ForEach(ls =>
                {
                    var learningSession = ls.Deck.LearningSessions.Where(l => l.UserId == Globals.CurrUser.Id).FirstOrDefault();
                    ls.Deck.LastReviewDate = learningSession.LastReview;
                    ls.Deck.OwnerName = Globals.ctx.Users.Find(ls.Deck.OwnerId).Name;
                    ls.Deck.ProgressBar = CalculateProgress(learningSession)[0];
                    ls.Deck.LevelsList = CalculateProgress(learningSession);
                    lvDecks.Items.Add(ls.Deck);
                });
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private double[] CalculateProgress(LearningSession learningSession)
        {
            double weightSum = 0;
            int quantity = 0;
            double cardWeight = 0;
            var session = learningSession.Session;
            double[] resultList = new double[6];
            learningSession.Progresses.ToList().ForEach(p =>
            {
                cardWeight = (p.Level == ((session + 1) > 9 ? session + 1 - 10 : session + 1)) ? 0.75 : cardWeight;
                cardWeight = (p.Level == ((session + 2) > 9 ? session + 2 - 10 : session + 2)) ? 0.75 : cardWeight;
                cardWeight = (p.Level == ((session + 3) > 9 ? session + 3 - 10 : session + 3)) ? 0.75 : cardWeight;
                cardWeight = (p.Level == ((session + 4) > 9 ? session + 4 - 10 : session + 4)) ? 0.75 : cardWeight;
                cardWeight = (p.Level == ((session + 5) > 9 ? session + 5 - 10 : session + 5)) ? 0.5 : cardWeight;
                cardWeight = (p.Level == ((session + 6) > 9 ? session + 6 - 10 : session + 6)) ? 0.5 : cardWeight;
                cardWeight = (p.Level == ((session + 7) > 9 ? session + 7 - 10 : session + 7)) ? 0.5 : cardWeight;
                cardWeight = (p.Level == ((session + 8) > 9 ? session + 8 - 10 : session + 8)) ? 0.25 : cardWeight;
                cardWeight = (p.Level == ((session + 9) > 9 ? session + 9 - 10 : session + 9)) ? 0.25 : cardWeight;
                cardWeight = (p.Level == ((session + 0) > 9 ? session + 0 - 10 : session + 0)) ? 0.25 : cardWeight;
                cardWeight = p.Level == -2 ? 1 : cardWeight;
                if (session == -1) { cardWeight = 0; }

                if (cardWeight == 0) resultList[1]++;
                if (cardWeight == 0.25) resultList[2]++;
                if (cardWeight == 0.5) resultList[3]++;
                if (cardWeight == 0.75) resultList[4]++;
                if (cardWeight == 1) resultList[5]++;

                weightSum += cardWeight;
                cardWeight = 0;
                quantity++;
            });
            resultList[0] = weightSum / quantity * 100;
            return resultList;
        }
        private void lvDecks_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            PlayDeck();
        }
        private void lvDecks_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvDecks.SelectedIndex == -1 || Globals.CurrUser == null || Globals.CurrUser.LearningSessions.Count == 0)
            {
                return;
            }
            CurrDeck = (Deck)lvDecks.SelectedItem;
        }
        private void ButtonDeleteDeck_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CurrDeck.OwnerId != Globals.CurrUser.Id)
                {
                    if (MessageBoxResult.No == MessageBox.Show("Remove the Deck?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning))
                    { return; }
                    var ls = CurrDeck.LearningSessions.Where(l => l.UserId == Globals.CurrUser.Id).FirstOrDefault();
                    ls.Progresses.Clear();
                    CurrDeck.LearningSessions.Remove(ls);
                    Globals.ctx.SaveChanges();
                    FetchUserDecks(Globals.CurrUser);
                    return;
                }

                if (!Utils.ShowMessge("Delete deck? The deck cannot be restored.", "Warning", Globals.Mw))
                { return; }
                CurrDeck.LearningSessions.Clear();
                CurrDeck.Cards.ToList().ForEach(c => c.Progresses.Clear());
                CurrDeck.Cards.Clear();
                Globals.CurrUser.Decks.Remove(CurrDeck);
                Globals.ctx.SaveChanges();
                FetchUserDecks(Globals.CurrUser);
            }
            catch (SystemException ex)
            {
                Utils.ShowMessge(ex.Message, "Database operation failed", Globals.Mw);
            }
        }
        private void ButtonPlay_Click(object sender, RoutedEventArgs e)
        {
            PlayDeck();
        }
        public void PlayDeck()
        {
            if (CurrDeck.CardsInDeck == 0)
            {
                Utils.ShowMessge("No card in the deck to learn.", "Information", Globals.Mw);
                return;
            }
            ReviewDialog reviewDialog = new ReviewDialog(CurrDeck, Globals.CurrUser) { Owner = Globals.Mw };
            if (reviewDialog.ShowDialog() == false)
            {
                Utils.UndoAll(Globals.ctx);
            }
            FetchUserDecks(Globals.CurrUser);
        }
        private void ButtonLearn_Click(object sender, RoutedEventArgs e)
        {
            CurrDeck = (Deck)lvDecks.SelectedItem;

            if (CurrDeck != null)
            {
                if (CurrDeck.CardsInDeck == 0)
                {
                    Utils.ShowMessge("No card in the deck to learn.", "Information", Globals.Mw);
                    return;
                }
                LearnDialog learnDialog = new LearnDialog(CurrDeck) { Owner = Globals.Mw };
                learnDialog.ShowDialog();
                FetchUserDecks(Globals.CurrUser);
            }
        }

        private void ButtonEdit_Click(object sender, RoutedEventArgs e)
        {
            if (CurrDeck.OwnerId != Globals.CurrUser.Id)
            {
                Utils.ShowMessge("Сannot modify the decks you didn't create", Globals.Mw);
                return;
            }
            ViewDeckDialog viewDeckDialog = new ViewDeckDialog(CurrDeck) { Owner = Globals.Mw };
            viewDeckDialog.ShowDialog();
            FetchUserDecks(Globals.CurrUser);
        }

        private void ButtonRestart_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!Utils.ShowMessge("Reset deck progress?", Globals.Mw))
                { return; }
                var learningSession = CurrDeck.LearningSessions.Where(l => l.UserId == Globals.CurrUser.Id).FirstOrDefault();
                learningSession.Session = -1;
                learningSession.LastReview = DateTime.Now;
                learningSession.Progresses.ToList().ForEach(p => p.Level = -1);
                Globals.ctx.SaveChanges();
                FetchUserDecks(Globals.CurrUser);
            }
            catch (SystemException ex)
            {
                Utils.ShowMessge(ex.Message, "Database operation failed", Globals.Mw);
            }
        }
        private void ButtonChart_Click(object sender, RoutedEventArgs e)
        {
            Globals.Mw.GridCommonSpace.Children.Clear();
            ChartUserControl chartUserControl = new ChartUserControl(CurrDeck);
            Globals.Mw.GridCommonSpace.Children.Add(chartUserControl);
        }

        private void PackIconMaterial_MouseDown_Play(object sender, MouseButtonEventArgs e)
        {
            PlayDeck();
        }

        private void Button_Click_Edit(object sender, RoutedEventArgs e)
        {
            if (CurrDeck.OwnerId != Globals.CurrUser.Id)
            {
                Utils.ShowMessge("Сannot modify the decks you didn't create", Globals.Mw);
                return;
            }
            ViewDeckDialog viewDeckDialog = new ViewDeckDialog(CurrDeck) { Owner = Globals.Mw };
            viewDeckDialog.ShowDialog();
            FetchUserDecks(Globals.CurrUser);
        }

        private void Button_Click_Chart(object sender, RoutedEventArgs e)
        {
            Globals.Mw.GridCommonSpace.Children.Clear();
            ChartUserControl chartUserControl = new ChartUserControl(CurrDeck);
            Globals.Mw.GridCommonSpace.Children.Add(chartUserControl);
        }

        private void Button_Click_Delete(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CurrDeck.OwnerId != Globals.CurrUser.Id)
                {
                    if (!Utils.ShowMessge("Remove the Deck?", "Warning", Globals.Mw))
                    { return; }
                    var ls = CurrDeck.LearningSessions.Where(l => l.UserId == Globals.CurrUser.Id).FirstOrDefault();
                    ls.Progresses.Clear();
                    CurrDeck.LearningSessions.Remove(ls);
                    Globals.ctx.SaveChanges();
                    FetchUserDecks(Globals.CurrUser);
                    return;
                }

                if (!Utils.ShowMessge("Delete deck? The deck cannot be restored.", "Warning", Globals.Mw))
                { return; }
                CurrDeck.LearningSessions.Clear();
                CurrDeck.Cards.ToList().ForEach(c => c.Progresses.Clear());
                CurrDeck.Cards.Clear();
                Globals.CurrUser.Decks.Remove(CurrDeck);
                Globals.ctx.SaveChanges();
                FetchUserDecks(Globals.CurrUser);
            }
            catch (SystemException ex)
            {
                Utils.ShowMessge(ex.Message, "Database operation failed", Globals.Mw);
            }
        }

        private void Button_Click_Restart(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!Utils.ShowMessge("Reset deck progress?", Globals.Mw))
                { return; }
                var learningSession = CurrDeck.LearningSessions.Where(l => l.UserId == Globals.CurrUser.Id).FirstOrDefault();
                learningSession.Session = -1;
                learningSession.LastReview = DateTime.Now;
                learningSession.Progresses.ToList().ForEach(p => p.Level = -1);
                Globals.ctx.SaveChanges();
                FetchUserDecks(Globals.CurrUser);
            }
            catch (SystemException ex)
            {
                Utils.ShowMessge(ex.Message, "Database operation failed", Globals.Mw);
            }
        }

        private void Button_Click_View(object sender, RoutedEventArgs e)
        {
            CurrDeck = (Deck)lvDecks.SelectedItem;

            if (CurrDeck != null)
            {
                if (CurrDeck.CardsInDeck == 0)
                {
                    Utils.ShowMessge("No cards in the deck to learn.", "Information", Globals.Mw);
                    return;
                }
                LearnDialog learnDialog = new LearnDialog(CurrDeck) { Owner = Globals.Mw };
                learnDialog.ShowDialog();
                FetchUserDecks(Globals.CurrUser);
            }
        }

        private void Button_Click_Learn(object sender, RoutedEventArgs e)
        {
            PlayDeck();
        }


    }
}
