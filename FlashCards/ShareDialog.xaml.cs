﻿using System;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;

namespace FlashCards
{
    /// <summary>
    /// Interaction logic for ShareDialog.xaml
    /// </summary>
    public partial class ShareDialog : Window
    {

        public ShareDialog()
        {
            InitializeComponent();
        }

        private void Button_SendEmail_click(object sender, RoutedEventArgs e)
        {
            // "info.flashcards.game@gmail.com", "TeamWork1" // Email and Password

            // regex for email
            string email = shareDialog_tbEmail.Text;
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(email);

            try
            {
                if (!match.Success)
                {
                    Utils.ShowMessge("Wrong email or empty field. Example: lettersNumbers@letters.letters", this);
                    return;
                }
                MailMessage msg = new MailMessage("info.flashcards.game@gmail.com", shareDialog_tbEmail.Text, "Welcome to flash cards application",
                              "Dear frinds we gladly want to share with you our memory trainer. <br /><br />It will be a pleasure for us to see you in our friendly community. <br /><br /> If you are interested in you can download it by following this <br /><br /> link: https://en.softonic.com/download/anki/windows/post-download <br /><br />Best Regards, <br /><br /> Flash Cards team");
                msg.IsBodyHtml = true;
                SmtpClient sc = new SmtpClient("smtp.gmail.com", 587);
                sc.UseDefaultCredentials = false;
                NetworkCredential networkCredintials = new NetworkCredential("info.flashcards.game@gmail.com",
                    "TeamWork1");

                sc.Credentials = networkCredintials;
                sc.EnableSsl = true;

                sc.Send(msg);
                Utils.ShowMessge("Email was sent", this);
                Close();
            }
            catch (SystemException ex)
            {
                Utils.ShowMessge(ex.Message, this);
            }
            catch (SmtpException ex)
            {
                Utils.ShowMessge(ex.Message, this);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void shareDialog_tbEmail_GotFocus(object sender, RoutedEventArgs e)
        {
            if (shareDialog_tbEmail.Text == "Email")
                shareDialog_tbEmail.Clear();
        }

        private void shareDialog_tbEmail_LostFocus(object sender, RoutedEventArgs e)
        {
            if (shareDialog_tbEmail.Text == "")
            {
                shareDialog_tbEmail.Text = "Email";
            }
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }
    }
}



